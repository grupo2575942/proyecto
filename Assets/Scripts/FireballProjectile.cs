using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class FireballProjectile : MonoBehaviour
{
    public float damage;
    public float speed;

    void Start()
    {
        
    }

    void Update()
    {
        StartCoroutine(DestroyObject());

        gameObject.transform.TransformDirection(Vector3.forward);
        gameObject.transform.Translate(new Vector3(0,0, speed * Time.deltaTime));

    }

    IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(3);
        Destroy(gameObject);
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("EnemyMinion") || other.gameObject.CompareTag("EnemyTurret"))
        {
            ObjetiveStats targetStats = other.gameObject.GetComponent<ObjetiveStats>();
            if (targetStats != null)
            {
                targetStats.TakeDamage(other.gameObject, damage);
            }
            Destroy(gameObject);
        }
    }
}
