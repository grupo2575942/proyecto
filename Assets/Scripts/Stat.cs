using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Stat : MonoBehaviour
{
    [Header("Base Stats")]
    public float health;
    public float damage;

    public float damageLerpDuration;
    public float currentHealth;
    private float targetHealth;
    private Coroutine damageCoroutine;

    HealthUI healthUI;

    private void Awake()
    {
        healthUI = GetComponent<HealthUI>();

        currentHealth = health;
        targetHealth = health;

        healthUI.Start3DSlider(health);
    }

    public void TakeDamage(GameObject target, float damageAmount)
    {
        Stat targetstat = target.GetComponent<Stat>();

        targetstat.targetHealth -= damageAmount;

        if (target.CompareTag("AllyMinion") && targetstat.targetHealth <= 0)
        {
            Destroy(target.gameObject);
        }
        else if(target.CompareTag("Turret") && targetstat.targetHealth <= 0)
        {
            Destroy(target.gameObject);
        }
        else if (target.CompareTag("EnemyMinion") && targetstat.targetHealth <= 0)
        {
            Destroy(target.gameObject);
        }
        else if (targetstat.damageCoroutine == null)
        {
            targetstat.StartLerpHealth();
        }

    }

    private void StartLerpHealth()
    {
        if (damageCoroutine == null)
        {
            damageCoroutine = StartCoroutine(LerpHealth());
        }
    }

    private IEnumerator LerpHealth()
    {
        float elapsepTime = 0;
        float initialHealth = currentHealth;
        float target = targetHealth;

        while (elapsepTime < damageLerpDuration)
        {
            currentHealth = Mathf.Lerp(initialHealth, target, elapsepTime / damageLerpDuration);
            UpdateHealthUI();

            elapsepTime += Time.deltaTime;
            yield return null;
        }

        currentHealth = target;
        UpdateHealthUI();

        damageCoroutine = null;
    }

    private void UpdateHealthUI()
    {
        healthUI.Update3DSlider(currentHealth);
    }
    void Start()
    {
        
    }

    
    void Update()
    {
        
    }
}
