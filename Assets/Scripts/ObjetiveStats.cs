using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ObjetiveStats : MonoBehaviour
{
    [Header("Base Stats")]
    public float health;
    public float damage;

    public float damageLerpDuration;
    public float currentHealth;
    private float targetHealth;
    private Coroutine damageCoroutine;

    HealthUI healthUI;

    private void Awake()
    {
        healthUI = GetComponent<HealthUI>();

        currentHealth = health;
        targetHealth = health;

        healthUI.Start3DSlider(health);
    }

    public void TakeDamage(GameObject target, float damageAmount)
    {
        ObjetiveStats targetStats = target.GetComponent<ObjetiveStats>();
        
        targetStats.targetHealth -= damageAmount;

        if (target.CompareTag("AllyMinion") && targetStats.targetHealth <= 0)
        {
            Destroy(target.gameObject);
        }
        else if (target.CompareTag("EnemyMinion") && targetStats.targetHealth <= 0)
        {
            Destroy(target.gameObject);
        }
        else if (target.CompareTag("AllyTurret") && targetStats.targetHealth <= 0)
        {
            Destroy(target.gameObject);
            SceneManager.LoadScene("Scene02");
        }
        else if (target.CompareTag("EnemyTurret") && targetStats.targetHealth <= 0)
        {
            Destroy(target.gameObject);
            SceneManager.LoadScene("Scene03");
        }
        else if (targetStats.damageCoroutine == null)
        {
            targetStats.StartLerpHealth();
        }

    }

    private void StartLerpHealth()
    {
        if (damageCoroutine == null)
        {
            damageCoroutine = StartCoroutine(LerpHealth());
        }
    }

    private IEnumerator LerpHealth()
    {
        float elapsepTime = 0;
        float initialHealth = currentHealth;
        float target = health;

        while (elapsepTime < damageLerpDuration)
        {
            currentHealth = Mathf.Lerp(initialHealth, targetHealth, elapsepTime / damageLerpDuration);
            UpdateHealthUI();

            elapsepTime += Time.deltaTime;
            yield return null;
        }

        currentHealth = targetHealth;
        UpdateHealthUI();

        damageCoroutine = null;
    }

    private void UpdateHealthUI()
    {
        healthUI.Update3DSlider(currentHealth);
    }

    void Start()
    {

    }


    void Update()
    {

    }
}
