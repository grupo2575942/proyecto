using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionRangedProjectile : MonoBehaviour
{
    private GameObject target;
    private float damage;
    public float speed = 10;

    public void SetTarget(GameObject newTarget, float attackDamage)
    {
        target = newTarget;
        damage = attackDamage;
    }
    void Start()
    {
        
    }

    
    void Update()
    {
        if ( target != null)
        {
            Vector3 dir = target.transform.position - transform.position;
            float distanceThisFrame = speed * Time.deltaTime;
            transform.Translate(dir.normalized * distanceThisFrame, Space.World);

            if (dir.magnitude <= distanceThisFrame )
            {
                DamageTarget();
                Destroy(gameObject);
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }


    void DamageTarget()
    {
        if (target.CompareTag("EnemyMinion") || target.CompareTag("EnemyTurret") || target.CompareTag("AllyMinion") || target.CompareTag("AllyTurret"))
        {
            ObjetiveStats targetStats = target.GetComponent<ObjetiveStats>();
            if (targetStats != null)
            {
                targetStats.TakeDamage(target, damage);
            }
        }
    }
}
