using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Runtime.InteropServices.WindowsRuntime;
using TreeEditor;
using UnityEngine;
using UnityEngine.AI;

public class MinionAI : MonoBehaviour
{
    private NavMeshAgent agent;
    public Transform currentTarget;
    public string enemyMinionTag = "EnemyMinion";
    public string turretTag = "EnemyTurret";
    public float stopDistance = 3.0f;
    public float aggroRange = 5.0f;
    public float targetSwitchInterval = 2.0f;

    private float timeSinceLastTargetSwitch = 0.0f;

    public bool IsInCombat { get; set; }
    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        FindAndSetTarget();
    }

    
    private void Update()
    {
        timeSinceLastTargetSwitch += Time.deltaTime;

        if (timeSinceLastTargetSwitch >= targetSwitchInterval)
        {
            CheckAndSwitchTargets();
            timeSinceLastTargetSwitch = 0.0f;
        }

        if (currentTarget != null)
        {
            Vector3 directionToTarget = currentTarget.position - transform.position;

            Vector3 stoppingPosition = currentTarget.position - directionToTarget.normalized * stopDistance;

            agent.SetDestination(stoppingPosition);

            FaceTarget();
        }
    }

    private void FaceTarget()
    {
        Vector3 directionToTarget = (currentTarget.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(directionToTarget.x, 0, directionToTarget.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    private void CheckAndSwitchTargets()
    {
        GameObject[] enemyMinions = GameObject.FindGameObjectsWithTag(enemyMinionTag);
        Transform closestEnemyMinion = GetClosesObjectInRadius(enemyMinions, aggroRange);

        if (closestEnemyMinion != null)
        {
            currentTarget = closestEnemyMinion;
        }
        else
        {
            GameObject[] turrets = GameObject.FindGameObjectsWithTag(turretTag);
            currentTarget = GetClosestObject(turrets);
        }
    }



    private Transform GetClosestObject(GameObject[] objects)
    {
        float closestDistance = Mathf.Infinity;
        Transform closestObject = null;
        Vector3 currentPosition = transform.position;

        foreach (GameObject obj in objects)
        {
            float distance = Vector3.Distance(currentPosition, obj.transform.position);

            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestObject = obj.transform;
            }
        }

        return closestObject;
    }

    private Transform GetClosesObjectInRadius(GameObject[] objects, float radius)
    {
        float closestDistance = Mathf.Infinity;
        Transform closestObject = null;
        Vector3 currentPosition = transform.position;

        foreach (GameObject obj in objects)
        {
            float distance = Vector3.Distance(currentPosition, obj.transform.position);

            if (distance < closestDistance && distance <= radius)
            {
                closestDistance = distance;
                closestObject = obj.transform;
            }
        }

        return closestObject;
    }

    private void FindAndSetTarget()
    {
        GameObject[] enemyMinions = GameObject.FindGameObjectsWithTag(enemyMinionTag);

        Transform closestEnemyMinion = GetClosesObjectInRadius(enemyMinions, aggroRange);

        if (closestEnemyMinion != null)
        {
            currentTarget = closestEnemyMinion;
        }
        else
        {
            GameObject[] turrets = GameObject.FindGameObjectsWithTag(turretTag);
            currentTarget = GetClosestObject(turrets);
        }
    }

}
