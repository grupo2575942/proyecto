using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

public class Turret : MonoBehaviour
{
    public float attackRange = 10f;
    public float attackCoolDown = 3f;

    public GameObject projectilePrefab;
    public Transform spawnPoint;
    public LineRenderer lineRenderer;

    private float nextAttackTime = 0f;
    private GameObject currentTarget;
    

    void Start()
    {
        
    }

    
    void Update()
    {  
       if (currentTarget == null || Vector3.Distance(transform.position, currentTarget.transform.position) > attackRange)
        {
            if (gameObject.CompareTag("AllyTurret"))
            {
                FindEnemyTargets();
            }
            if (gameObject.CompareTag("EnemyTurret"))
            {
                FindAllyTargets();
            }
        }

        UpdateLineToCurrentTarget();

        if (Time.time >= nextAttackTime)
        {
            AttackCurrentTarget();
            nextAttackTime = Time.time + attackCoolDown;
        }     
  
    }
    
    private void FindEnemyTargets()
    {
        GameObject[] minions = GameObject.FindGameObjectsWithTag("EnemyMinion");
        float closestDistance = float.MaxValue;

        foreach (GameObject minion in minions)
        {
            float distance = Vector3.Distance(transform.position, minion.transform.position);
            if (distance <= attackRange && distance < closestDistance)
            {
                closestDistance = distance;
                currentTarget = minion;
            }
        }
    }

    private void FindAllyTargets()
    {
        GameObject[] minions = GameObject.FindGameObjectsWithTag("AllyMinion");
        float closestDistance = float.MaxValue;

        foreach(GameObject minion in minions)
        {
            float distance = Vector3.Distance(transform.position, minion.transform.position);
            if (distance <= attackRange && distance < closestDistance)
            {
                closestDistance = distance;
                currentTarget = minion;
            }
        }
    }
    
    private void UpdateLineToCurrentTarget()
    {
        if (currentTarget != null)
        {
            lineRenderer.enabled = true;
            lineRenderer.SetPosition(0, spawnPoint.position);
            lineRenderer.SetPosition(1, currentTarget.transform.position);
        }
        else
        {
            lineRenderer.enabled = false;
        }
    }

    private void AttackCurrentTarget()
    {
        if (currentTarget != null)
        {
            GameObject projectileGO = Instantiate(projectilePrefab, spawnPoint.position, Quaternion.identity);
            Projectile projectile = projectileGO.GetComponent<Projectile>();
            projectile.Seek(currentTarget.transform);
        }
    }
}
