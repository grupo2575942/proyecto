using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed = 5f;
    private Transform target;

    private ObjetiveStats towerStats;

    void Start()
    {
        towerStats = GameObject.FindGameObjectWithTag("AllyTurret").GetComponent<ObjetiveStats>();
    }

    public void Seek(Transform _target)
    {
        target = _target;
    }
    
    private void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
    }

    private void HitTarget()
    {
        ObjetiveStats targetStats = target.gameObject.GetComponent<ObjetiveStats>();
        targetStats?.TakeDamage(target.gameObject, towerStats.damage);

        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == target.gameObject)
        {
            HitTarget();
            
            Destroy(gameObject);
        }
    }
}
