using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class MinionMeleeCombat : MonoBehaviour
{
    private float attackRange = 1.0f;
    private float attackDamage = 10.0f;
    public float attackCoolDown = 1.0f;

    private float lastAttackTime;
    private bool isAttacking = false;

    private ObjetiveStats stats;
    private MinionAI minionAI;

    private void Start()
    {
        stats = GetComponent<ObjetiveStats>();
        minionAI = GetComponent<MinionAI>();

        attackRange = minionAI.stopDistance + 0.5f;
        attackDamage = stats.damage;
    }
    private void Update()
    {
        if (isAttacking)
        {
            if (Time.time - lastAttackTime >= attackCoolDown)
            {
                isAttacking = false;
                minionAI.IsInCombat = false;
            }
        }

        if (CanAttack())
        {
            Attack();
        }
        else
        {
            minionAI.IsInCombat = false;
        }
    }

    private bool CanAttack()
    {
        if (!isAttacking && minionAI.currentTarget != null)
        {
            float distanceToTarget = Vector3.Distance(transform.position, minionAI.currentTarget.position);
            return distanceToTarget <= attackRange;
        }
        return false;
    }
    private void Attack()
    {
        lastAttackTime = Time.time;
        isAttacking = true;
        minionAI.IsInCombat = true;
        
        if (minionAI.currentTarget.CompareTag("EnemyMinion"))
        {
            stats.TakeDamage(minionAI.currentTarget.gameObject, attackDamage);
        }

        if (minionAI.currentTarget.CompareTag("AllyMinion"))
        {
            stats.TakeDamage(minionAI.currentTarget.gameObject, attackDamage);
        }
    }
}
