using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class MinionRangedCombat : MonoBehaviour
{
    public GameObject projectilePrefab;
    public Transform projectileSpawnPoint;

    private float attackRange = 10.0f;
    private float attackDamage = 10.0f;
    public float attackCoolDown = 1.0f;

    private float lastAttackTime;
    private bool isAttacking = false;

    private ObjetiveStats stats;
    private MinionAI minionAI;

    void Start()
    {
        stats = GetComponent<ObjetiveStats>();
        minionAI = GetComponent<MinionAI>();

        attackRange = minionAI.stopDistance + 0.5f;
        attackDamage = stats.damage;
    }

    
    void Update()
    {
        if (isAttacking)
        {
            if ( Time.time - lastAttackTime >= attackCoolDown)
            {
                isAttacking = false;
                minionAI.IsInCombat = false;
            }
        }

        if (CanAttack())
        {
            Attack();
        }
        else
        {
            minionAI.IsInCombat = false;
        }
    }

    private bool CanAttack()
    {
        if(!isAttacking && minionAI.currentTarget != null)
        {
            float distanceToTarget = Vector3.Distance(transform.position, minionAI.currentTarget.position);
            return distanceToTarget <= attackRange;
        }
        return false;
    }

    private void Attack()
    {
        lastAttackTime = Time.time;
        isAttacking = true;
        minionAI.IsInCombat = true;

        GameObject projectile = Instantiate(projectilePrefab, projectileSpawnPoint.position, Quaternion.identity);
        MinionRangedProjectile projectileScript = projectile.GetComponent<MinionRangedProjectile>();
        projectileScript.SetTarget(minionAI.currentTarget.gameObject, attackDamage);
    }
}
