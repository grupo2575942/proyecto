using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.Pool;

public class MinionSpawner : MonoBehaviour
{
    public float meleeMinionMoveSpeed;
    public float rangedMinionMoveSpeed;
    public float superMinionMoveSpeed;

    public GameObject meleeMinionPrefab;
    public GameObject rangedMinionPrefab;
    public GameObject superMinionPrefab;

    public Transform[] spawnPoints;

    public float spawnInterval = 20.0f;
    public int minionsPerWave = 6;
    public int wavesUntilSuperMinion = 3;
    private int waveCount = 0;

    public float delayBetweenMinions;

    void Start()
    {
        StartCoroutine(SpawnMinions());
    }

    
    void Update()
    {
        
    }

    private IEnumerator SpawnMinions()
    {
        while (true)
        {
            waveCount++;

            if (waveCount % wavesUntilSuperMinion == 0)
            {
                for (int i = 0; i < minionsPerWave - 1; i++)
                {
                    if (i < 3)
                        SpawnRegularMinion(meleeMinionPrefab, meleeMinionMoveSpeed);
                    else
                        SpawnRegularMinion(rangedMinionPrefab, rangedMinionMoveSpeed);

                    yield return new WaitForSeconds(delayBetweenMinions);
                }

                SpawnRegularMinion(meleeMinionPrefab, meleeMinionMoveSpeed);
                yield return new WaitForSeconds(delayBetweenMinions);

                SpawnSuperMinion();
                yield return new WaitForSeconds(spawnInterval - delayBetweenMinions * (minionsPerWave - 1) - delayBetweenMinions);
            }
            else
            {
                for (int i = 0; i < minionsPerWave; i++)
                {
                    if (i < 3)
                        SpawnRegularMinion(meleeMinionPrefab, meleeMinionMoveSpeed);
                    else
                        SpawnRegularMinion(rangedMinionPrefab, rangedMinionMoveSpeed);

                    yield return new WaitForSeconds(delayBetweenMinions);
                }
                yield return new WaitForSeconds(spawnInterval - delayBetweenMinions * minionsPerWave);
            }
        }
    }

    private void SpawnRegularMinion(GameObject minionPrefab, float moveSpeed)
    {
        Transform spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];
        GameObject minion = Instantiate(minionPrefab, spawnPoint.position, spawnPoint.rotation);

        UnityEngine.AI.NavMeshAgent minionAgent = minion.GetComponent<UnityEngine.AI.NavMeshAgent>();
        minionAgent.speed = moveSpeed;
    }

    private void SpawnSuperMinion()
    {
        Transform spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];
        GameObject superMinion = Instantiate(superMinionPrefab, spawnPoint.position, spawnPoint.rotation);

        UnityEngine.AI.NavMeshAgent superMinionAgent = superMinion.GetComponent<UnityEngine.AI.NavMeshAgent>();
        superMinionAgent.speed = superMinionMoveSpeed;
    }
}
